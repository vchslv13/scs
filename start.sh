#!/bin/bash
if [[ "$1" == "start_crawling" ]]; then
    exec ./start_crawling.py /feeds
else
    exec uwsgi --ini scs.ini
fi
