FROM python:3.7-slim-buster

ARG HOMEDIR=/app
WORKDIR ${HOMEDIR}

RUN apt-get update && apt-get install -y gcc
COPY requirements.txt ${HOMEDIR}
RUN pip install -r requirements.txt

RUN useradd -d ${HOMEDIR} -N -g users app
USER app

COPY . ${HOMEDIR}
ENV SCS_CONFIG=${HOMEDIR}/scs_conf_prod.py

ENTRYPOINT ["./start.sh"]
