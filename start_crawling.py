#!/usr/bin/env python3
import os
import sys
import requests
import time
import subprocess
from datetime import datetime
from datetime import timedelta
import logging
from urllib.parse import urljoin


"""
Script takes 2 mandatory arguments:
1 -- scrapy project path
2 -- feeds root directory path 
"""

SCS_URL = 'http://scs:8000/scs/'
SCRAPYD_URL = 'http://scrapyd:6800/'


class AsanaBot:
    token = '0/1aa382c57603779ab03c9fa911768856'
    assignee = 24541667078193
    project = 372433341146741
    _sent_critical = False

    def add_task(self, name, description=None, due_date=None):
        data = dict()
        data['assignee'] = self.assignee
        data['projects'] = self.project
        data['name'] = name
        if description:
            data['notes'] = description
        if due_date:
            data['due_on'] = due_date

        headers = {'Authorization': 'Bearer {}'.format(self.token)}
        r = requests.post('https://app.asana.com/api/1.0/tasks', headers=headers, data=data)

        return r

    def add_fix_task(self, spider):
        tomorrow = (datetime.today() + timedelta(days=1)).isoformat()
        return self.add_task('Fix: {}'.format(spider), due_date=tomorrow)

    def add_critical_scraping_problem(self):
        if not self._sent_critical:
            self._sent_critical = True
            return self.add_task('Fix (critical): problem with scraping control')


def processFinishedSpider(spider, timestamp, finishedJobs, prev_status):
    job_stats = dict()
    job_stats['name'] = spider
    job_stats['timestamp'] = timestamp

    try:
        job_stats['amount'] = len(open(os.path.join(csvFolderPath, spider + '/' + spider + '_unchecked.csv'), 'rb')
                                  .readlines())
    except IOError as e:
        logging.error('Got "%s" while counting %s results!', e, spider)
        job_stats['amount'] = 0

    r = requests.post(urljoin(SCS_URL, 'add_stats'), json=job_stats)
    if r.status_code != 200:
        asanaBot.add_critical_scraping_problem()

    time.sleep(2)

    # rename the successful results
    r = requests.get(urljoin(SCS_URL, 'get_script_statuses'))
    if r.status_code == 200:
        status = r.json()[spider]
    else:
        status = None
        asanaBot.add_critical_scraping_problem()

    if status == 'checked' or status == 'first-run':
        try:
            filePath = os.path.join(csvFolderPath, spider + '/' + spider + '.csv')
            os.remove(filePath)
            logging.debug('Results file %s was removed.', filePath)
        except OSError:
            logging.error('Found no previous %s results!', spider)
        try:
            os.rename(os.path.join(csvFolderPath, spider + '/' + spider + '_unchecked.csv'),
                      os.path.join(csvFolderPath, spider + '/' + spider + '.csv'))
        except OSError as e:
            logging.error('Got "%s" while renaming %s results!', e, spider)
    elif prev_status == 'one-time-fail' and status == 'fix-needed':
        asanaBot.add_fix_task(spider)

    finishedJobs.append(spider)


asanaBot = AsanaBot()

startTime = datetime.now()
startTimeSec = time.time()
scrapingInterval = 11 * 60 * 60

projectName = 'DesFlow'
csvFolderPath = os.path.expanduser(sys.argv[1])

logging.basicConfig(format='%(asctime)s: %(message)s',
                    datefmt='%d.%m.%Y %H:%M:%S', level=logging.DEBUG)
logging.debug('Current working directory is %s', os.getcwd())

# Get the list of unfinished jobs from the previous scraping
unfinishedSpiders = list()
r = requests.get(urljoin(SCS_URL, 'get_script_statuses'))
if r.status_code == 200:
    prev_statuses = r.json()
    unfinishedSpiders = [name for name in prev_statuses if prev_statuses[name] == 'unfinished']
else:
    asanaBot.add_critical_scraping_problem()

# Delete all previous versions of the projects folder and deploy the new one
versPage = requests.get(urljoin(SCRAPYD_URL, 'listversions.json?project={0}'.format(projectName)))
versList = versPage.json()['versions']
for version in versList[:-1]:
    params = dict()
    params['project'] = projectName
    params['version'] = version
    ans = requests.post(urljoin(SCRAPYD_URL, 'delversion.json'), data=params)
    logging.debug('Deleted version %s with answer %s', version, ans.text)

# Schedule the spiders
spiderPage = requests.get(urljoin(SCRAPYD_URL, 'listspiders.json?project={0}'.format(projectName)))
spiderList = spiderPage.json()['spiders']
spiderList = [spider for spider in spiderList if spider not in unfinishedSpiders]

# Sheduling
sheduledJobs = dict()
for spider in spiderList:
    # remove previous unchecked results
    try:
        filePath = os.path.join(csvFolderPath, spider + '/' + spider + '_unchecked.csv')
        os.remove(filePath)
        logging.debug('Unchecked results file %s was removed.', spider)
    except OSError:
        pass

    params = dict()
    params['project'] = projectName
    params['spider'] = spider
    params['setting'] = ('FEED_URI=file://' + os.path.join(csvFolderPath, spider + '/' + spider + '_unchecked.csv'),
                         'FEED_FORMAT=csv')
    ans = requests.post(urljoin(SCRAPYD_URL, 'schedule.json'), data=params)
    logging.debug('Scheduled spider %s with answer %s', spider, ans.text)

    sheduledJobs[ans.json()['jobid']] = spider
time.sleep(3600)

# save info about just finished jobs
finishedJobsList = list()
while True:
    r = requests.get(urljoin(SCRAPYD_URL, 'listjobs.json?project={0}'.format(projectName)))
    jobs = r.json()
    finishedJobs = jobs['finished']
    for job in finishedJobs:
        job['timestamp'] = datetime.strptime(job['end_time'][:19], '%Y-%m-%d %H:%M:%S').timestamp()

    for job in finishedJobs:
        if job['spider'] not in finishedJobsList and (job['id'] in sheduledJobs or job['spider'] in unfinishedSpiders):
            prev_status = prev_statuses[job['spider']] if job['spider'] in prev_statuses else 'first-run'
            try:
                processFinishedSpider(job['spider'], job['timestamp'], finishedJobsList, prev_status)
            except Exception as e:
                logging.error('Got error "%s" while processing the spider %s', e, job['spider'])

    # finish script if it runs longer than the allowed time interval
    if time.time() - startTimeSec > scrapingInterval:
        break
    time.sleep(600)

# write the info about unfinished spiders
for spider in spiderList:
    if spider not in finishedJobsList:
        job_stats = dict()
        job_stats['name'] = spider
        job_stats['timestamp'] = time.time()
        job_stats['amount'] = -1
        r = requests.post(urljoin(SCS_URL, 'add_stats'), json=job_stats)

        if r.status_code != 200:
            asanaBot.add_critical_scraping_problem()
