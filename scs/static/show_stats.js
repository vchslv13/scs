function showPlot(name){
  if (window.rowWithStats) {
    rowWithStats.parentElement.removeChild(rowWithStats);
    rowWithStats = null;
  } else {
    var req = new XMLHttpRequest();
    req.addEventListener('load', function(){
      var stats = JSON.parse(this.responseText);
      var data = Object();
      data.x = stats.date.map(function(i){return new Date(i*1000);});
      data.y = stats.amount;
      data.type = 'scatter';
      var plotWithStats = document.createElement('div');

      rowWithStats = document.createElement('tr');
      rowWithStats.id = 'chart';

      var row = document.querySelector('tr[data-name="' + name + '"]')
      row.parentNode.insertBefore(rowWithStats, row.nextSibling)
      var td = rowWithStats.appendChild(document.createElement('td'))
      td.setAttribute('colspan', 5)
      td.appendChild(plotWithStats)

      Plotly.plot(plotWithStats, [data]);
    });
    req.open('GET', 'get_script_stats?name=' + name);
    req.send();
  }
}
