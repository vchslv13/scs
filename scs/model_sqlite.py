import sqlite3


class myDB():
    _path = None
    _conn = None

    @staticmethod
    def getConn():
        if not myDB._conn:
            myDB._conn = sqlite3.connect(myDB._path)
            curs = myDB.getConn().cursor()
            curs.execute("SELECT name FROM sqlite_master WHERE type = 'table'")
            if not len(curs.fetchall()):
                create_db()
        return myDB._conn

    @staticmethod
    def closeDB():
        if myDB._conn:
            myDB._conn.close()
            myDB._conn = None

    @staticmethod
    def setDataPath(p):
        myDB._path = p


def getStatus(name):
    curs = myDB.getConn().cursor()

    curs.execute("SELECT status_id FROM Spiders WHERE name = ?;", (name,))
    status_id = curs.fetchone()
    if not status_id:
        return 'first-run'

    curs.execute("SELECT name FROM Statuses WHERE id = ?;", (status_id[0],))
    return curs.fetchone()[0]


def getAllStatuses():
    curs = myDB.getConn().cursor()

    curs.execute("SELECT Spiders.name, Statuses.name FROM Spiders JOIN Statuses ON Statuses.id = Spiders.status_id;")
    res = {r[0]: r[1] for r in curs}

    return res


def setStatus(name, status):
    curs = myDB.getConn().cursor()
    curs.execute("UPDATE Spiders SET status_id = (SELECT id FROM Statuses WHERE name = ?) WHERE name = ?;",
                 (status, name))

    # set the last scraped items amount as the last checked if the status is "checked"
    if status == 'checked':
        curs.execute("INSERT OR REPLACE INTO Last_checked_amount (spider_id, amount_id) SELECT Stats.spider_id AS spider_id, "
                     "Stats.id AS amount_id FROM Stats "
                     "WHERE timestamp = (SELECT MAX(timestamp) FROM Stats WHERE spider_id = "
                     "(SELECT id FROM Spiders WHERE name = ?))"
                     "AND spider_id = (SELECT id FROM Spiders WHERE name = ?);", (name, name))
        setNumOfFails(name, 0)

    curs.connection.commit()


def getScriptsList():
    curs = myDB.getConn().cursor()
    curs.execute("SELECT name FROM Spiders;")
    res = [r[0] for r in curs]
    return res


def getAllStats():
    curs = myDB.getConn().cursor()
    curs.execute("SELECT Stats.timestamp, Spiders.name, Stats.amount "
                 "FROM Stats JOIN Spiders ON Spiders.id = Stats.spider_id;")

    # this nonsense is made for backwards compatibility, I`ll fix it later
    res = dict()
    for r in curs:
        if r[0] not in res:
            res[r[0]] = dict()
        res[r[0]][r[1]] = r[2]

    return res


def getStats(day):
    """Takes day as Unix-timestamp"""
    curs = myDB.getConn().cursor()
    curs.execute("SELECT Spiders.name, Stats.amount FROM Stats JOIN Spiders ON Spiders.id = Stats.spider_id "
                 "WHERE date(Stats.timestamp, 'unixepoch', 'localtime') = date(?, 'unixepoch', 'localtime');", (day,))
    res = {r[0]: r[1] for r in curs}
    return res


def setStats(data):
    curs = myDB.getConn().cursor()

    # get spider id and create new record if none
    curs.execute("SELECT id FROM Spiders WHERE name = ?;", (data['name'],))
    res = curs.fetchone()
    if res:
        spiderId = res[0]
    else:
        addNewScript(data['name'])
        curs.execute("SELECT id FROM Spiders WHERE name = ?;", (data['name'],))
        spiderId = curs.fetchone()[0]

    # get previous status and checked amount of items
    prevStatus = getStatus(data['name'])

    curs.execute("SELECT amount FROM Last_checked_amount JOIN Stats ON "
                 "Last_checked_amount.amount_id = Stats.id "
                 "WHERE Last_checked_amount.spider_id = ?", (spiderId,))
    res = curs.fetchone()
    prevAmount = res[0] if res else None

    # define new status of the spider
    maxFailsInRow = 2  # TODO: take it out into config file
    numOfFails = getNumOfFails(data['name'])

    if data['amount'] == -1:
        setStatus(data['name'], 'unfinished')
    elif prevStatus == 'fix-needed':
        pass
    elif prevAmount and prevAmount - data['amount'] > 0.2 * prevAmount:
        numOfFails += 1
        if numOfFails > maxFailsInRow or prevStatus == 'unfinished':
            setStatus(data['name'], 'fix-needed')
        else:
            setStatus(data['name'], 'one-time-fail')
            setNumOfFails(data['name'], numOfFails)
    elif not prevAmount and data['amount'] == 0:
        setStatus(data['name'], 'fix-needed')
    else:
        setStatus(data['name'], 'checked')

    # save new scraped items amount
    curs.execute("INSERT INTO Stats (spider_id, amount, timestamp) VALUES (?, ?, ?);",
                 (spiderId, data['amount'], data['timestamp']))
    curs.connection.commit()

    if getStatus(data['name']) in ['checked', 'first-run']:
        curs.execute("INSERT OR REPLACE INTO Last_checked_amount (spider_id, amount_id) SELECT Stats.spider_id AS spider_id, "
                     "Stats.id AS amount_id FROM Stats "
                     "WHERE timestamp = (SELECT MAX(timestamp) FROM Stats WHERE spider_id = ?)"
                     "AND spider_id = ?;", (spiderId, spiderId))
        curs.connection.commit()


def addNewScript(name):
    curs = myDB.getConn().cursor()
    curs.execute("SELECT id FROM Statuses WHERE name = 'first-run';")
    first_run_id = curs.fetchone()[0]
    curs.execute("INSERT INTO Spiders (name, status_id) VALUES (?, ?);", (name, first_run_id))
    curs.connection.commit()

    curs.execute("SELECT id FROM Spiders WHERE name = ?;", (name,))
    spider_id = curs.fetchone()[0]

    curs.execute("INSERT INTO Fails_in_row (amount, spider_id) VALUES (0, ?);", (spider_id,))
    curs.connection.commit()


def getNumOfFails(name):
    curs = myDB.getConn().cursor()
    curs.execute("SELECT amount FROM Fails_in_row WHERE spider_id = "
                 "(SELECT id FROM Spiders WHERE name = ?);", (name,))

    res = curs.fetchone()
    return res[0] if res else 0


def setNumOfFails(name, amount):
    curs = myDB.getConn().cursor()
    curs.execute("UPDATE Fails_in_row SET amount = ? WHERE spider_id = "
                 "(SELECT id FROM Spiders WHERE name = ?);", (amount, name))
    curs.connection.commit()


def closeDB():
    myDB.closeDB()


# creates SQLite DB with the predefined structure
def create_db():
    curs = myDB.getConn().cursor()
    curs.execute("CREATE TABLE Statuses (id INTEGER PRIMARY KEY ASC, name TEXT NOT NULL);")
    curs.execute('INSERT INTO Statuses (name) VALUES ("checked"), ("first-run"), ("fix-needed"), ("unfinished"), '
                 '("removed"), ("one-time-fail");')
    curs.execute("CREATE TABLE Spiders (id INTEGER PRIMARY KEY ASC, name TEXT NOT NULL, status_id INTEGER, "
                 "FOREIGN KEY (status_id) REFERENCES Statuses (id));")
    curs.execute("CREATE TABLE Stats (id INTEGER PRIMARY KEY ASC, spider_id INTEGER NOT NULL, amount INTEGER NOT NULL, "
                 "timestamp INTEGER NOT NULL, FOREIGN KEY (spider_id) REFERENCES Spiders (id));")
    curs.execute("CREATE TABLE Last_checked_amount (spider_id INT, amount_id INTEGER, "
                 "PRIMARY KEY (spider_id),"
                 "FOREIGN KEY (spider_id) REFERENCES Spiders (id),"
                 "FOREIGN KEY (amount_id) REFERENCES Stats (id))"
                 " WITHOUT ROWID;")
    curs.execute("CREATE TABLE Fails_in_row (id INTEGER PRIMARY KEY ASC, amount INTEGER NOT NULL, "
                 "spider_id INTEGER NOT NULL, FOREIGN KEY (spider_id) REFERENCES Spiders (id));")
    curs.connection.commit()
