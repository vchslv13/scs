from os import path
from . import model_sqlite as model
import time
from flask import Flask, request, abort, render_template
import json

app = Flask(__name__)
app.debug = True
app.config.from_envvar('SCS_CONFIG')
db_path = path.expanduser(app.config['DATABASE_PATH'])
model.myDB.setDataPath(db_path)


@app.teardown_request
def closeDB(err):
    model.closeDB()


@app.route('/add_stats', methods=['POST'])
def addStats():
    """Receives the json dict with stats for spider execution, which has such fields:
    timestamp, amount, name.
    """
    stats = request.get_json(cache=False)
    if not stats or 'timestamp' not in stats or 'amount' not in stats or 'name' not in stats:
        abort(400)
    model.setStats(stats)
    return 'Stats successfully added.'


@app.route('/show_stats')
def showStats():
    raw_stats = model.getStats(time.time())
    statuses = model.getAllStatuses()

    keys = [k for k in model.getScriptsList() if statuses[k] != 'removed']
    keys.sort()

    stats = [{'name': k, 'amount': raw_stats[k] if k in raw_stats else '-',
              'status': statuses[k]}
             for k in keys]
    return render_template('stats.html', stats=stats)


@app.route('/get_script_stats')
def getScriptStats():
    if 'name' not in request.args:
        abort(400)

    allStats = model.getAllStats()
    stats = {k: allStats[k][request.args['name']] for k in allStats
             if request.args['name'] in allStats[k]}
    stats = sorted(stats.items(), key=lambda i: i[0])

    data = dict()
    data['date'], data['amount'] = list(zip(*stats))

    return json.dumps(data)


@app.route('/get_script_statuses')
def getScriptStatuses():
    return json.dumps(model.getAllStatuses())


@app.route('/set_status', methods=['POST'])
def addStatus():
    data = request.get_json(cache=False)
    if 'name' not in data or 'status' not in data:
        abort(400)

    validStatuses = ['fix-needed', 'checked', 'first-run', 'unfinished', 'removed']
    if data['status'] not in validStatuses:
        abort(400)

    model.setStatus(data['name'], data['status'])

    return 'Status changed'
