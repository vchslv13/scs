import pytest
from . import scs

import os
import time
import json
import tempfile


@pytest.fixture(scope='module')
def app():
    scs.app.testing = True
    return scs.app.test_client()


@pytest.fixture(scope='module')
def db():
    db_fd, db_path = tempfile.mkstemp()
    scs.model.myDB.setDataPath(db_path)
    yield None
    os.close(db_fd)
    os.unlink(db_path)


def test_modelSetStats_getStats():
    with scs.app.app_context():
        scs.model.myDB.setDataPath(':memory:')
        testTimestamp = time.mktime(time.strptime('01 01 2099', '%d %m %Y'))
        scs.model.setStats({'timestamp': testTimestamp, 'name': 'test', 'amount': 2000})
        scs.model.setStats({'timestamp': testTimestamp + 24 * 3600, 'name': 'test', 'amount': 4000})

        assert scs.model.getStats(testTimestamp)
        assert scs.model.getStats(testTimestamp)['test'] == 2000
        assert scs.model.getStats(testTimestamp + 24 * 3600)['test'] == 4000


def test_addStats_setsStatusFirstRun(app, db):
    app.post('/add_stats', data=json.dumps({'name': 'test_status', 'amount': 1000, 'timestamp': time.time()}),
           headers={'Content-Type': 'application/json'})

    assert scs.model.getStatus('test_status') == 'first-run'


def test_addStats_setsStatusChecked_whenItemsAmountIsOk(app, db):
    testTimestamp = time.time()
    app.post('/add_stats', data=json.dumps({'name': 'spider_with_prev_res2', 'amount': 1000, 'timestamp': testTimestamp}),
           headers={'Content-Type': 'application/json'})
    app.post('/add_stats', data=json.dumps({'name': 'spider_with_prev_res2', 'amount': 1000, 'timestamp': testTimestamp + 24 * 3600}),
           headers={'Content-Type': 'application/json'})

    assert scs.model.getStatus('spider_with_prev_res2') == 'checked'


def test_setStatus_setsStatusChecked_whenStatusIsSetToCheckedManually(app, db):
    app.post('/add_stats', data=json.dumps({'name': 'test_status2', 'amount': 0, 'timestamp': time.time()}),
             headers={'Content-Type': 'application/json'})
    res = app.post('/set_status', data=json.dumps({'name': 'test_status2', 'status': 'checked'}),
                   headers={'Content-Type': 'application/json'})

    assert scs.model.getStatus('test_status2') == 'checked'
    assert res.status_code == 200


def test_addStats_setsStatusFixNeeded_whenFirstRunGotNoItems(app, db):
    app.post('/add_stats', data=json.dumps({'name': 'test3', 'amount': 0, 'timestamp': time.time()}),
             headers={'Content-Type': 'application/json'})

    assert scs.model.getStatus('test3') == 'fix-needed'


def test_setStatus_changesLastCheckedAmountToLastAmount_whenStatusCheckedIsSet(app, db):
    timestamp = time.time()
    app.post('/add_stats', data=json.dumps({'name': 'test4', 'amount': 1000, 'timestamp': timestamp}),
             headers={'Content-Type': 'application/json'})

    for i in range(3):
        timestamp += 24 * 3600
        app.post('/add_stats', data=json.dumps({'name': 'test4', 'amount': 600, 'timestamp': timestamp}),
                 headers={'Content-Type': 'application/json'})
    assert scs.model.getStatus('test4') == 'fix-needed'

    app.post('/set_status', data=json.dumps({'name': 'test4', 'status': 'checked'}),
             headers={'Content-Type': 'application/json'})
    timestamp += 24 * 3600
    app.post('/add_stats', data=json.dumps({'name': 'test4', 'amount': 600, 'timestamp': timestamp}),
             headers={'Content-Type': 'application/json'})
    assert scs.model.getStatus('test4') == 'checked'


def test_addStats_setsStatusFixNeeded_whenUnfinishedSpiderReturnsReducedAmountOfItems(app, db):
    timestamp = time.time()
    app.post('/add_stats', data=json.dumps({'name': 'test5', 'amount': 1000, 'timestamp': timestamp}),
             headers={'Content-Type': 'application/json'})
    timestamp += 24 * 3600
    app.post('/add_stats', data=json.dumps({'name': 'test5', 'amount': -1, 'timestamp': timestamp}),
             headers={'Content-Type': 'application/json'})
    assert scs.model.getStatus('test5') == 'unfinished'

    timestamp += 24 * 3600
    app.post('/add_stats', data=json.dumps({'name': 'test5', 'amount': 600, 'timestamp': timestamp}),
             headers={'Content-Type': 'application/json'})
    assert scs.model.getStatus('test5') == 'fix-needed'


def test_addStats_setsStatusChecked_whenUnfinishedSpiderReturnsNormalAmount(app, db):
    timestamp = time.time()
    app.post('/add_stats', data=json.dumps({'name': 'test6', 'amount': 1000, 'timestamp': timestamp}),
             headers={'Content-Type': 'application/json'})
    timestamp += 24 * 3600
    app.post('/add_stats', data=json.dumps({'name': 'test6', 'amount': -1, 'timestamp': timestamp}),
             headers={'Content-Type': 'application/json'})
    assert scs.model.getStatus('test6') == 'unfinished'

    timestamp += 24 * 3600
    app.post('/add_stats', data=json.dumps({'name': 'test6', 'amount': 1200, 'timestamp': timestamp}),
             headers={'Content-Type': 'application/json'})
    assert scs.model.getStatus('test6') == 'checked'

def test_addStats_setsOneTimeFail_whenCheckedSpiderFails(app, db):
    timestamp = time.time()
    app.post('/add_stats', data=json.dumps({'name': 'test7', 'amount': 1000, 'timestamp': timestamp}),
             headers={'Content-Type': 'application/json'})
    timestamp += 24 * 3600
    app.post('/add_stats', data=json.dumps({'name': 'test7', 'amount': 1000, 'timestamp': timestamp}),
             headers={'Content-Type': 'application/json'})
    timestamp += 24 * 3600
    app.post('/add_stats', data=json.dumps({'name': 'test7', 'amount': 500, 'timestamp': timestamp}),
             headers={'Content-Type': 'application/json'})
    assert scs.model.getStatus('test7') == 'one-time-fail'

def test_addStats_setsStatusChecked_whenOneTimeFailSpiderReturnsNormalAmount(app, db):
    timestamp = time.time()
    app.post('/add_stats', data=json.dumps({'name': 'test9', 'amount': 1000, 'timestamp': timestamp}),
             headers={'Content-Type': 'application/json'})
    timestamp += 24 * 3600
    app.post('/add_stats', data=json.dumps({'name': 'test9', 'amount': 1000, 'timestamp': timestamp}),
             headers={'Content-Type': 'application/json'})
    timestamp += 24 * 3600
    app.post('/add_stats', data=json.dumps({'name': 'test9', 'amount': 500, 'timestamp': timestamp}),
             headers={'Content-Type': 'application/json'})
    assert scs.model.getStatus('test9') == 'one-time-fail'

    timestamp += 24 * 3600
    app.post('/add_stats', data=json.dumps({'name': 'test9', 'amount': 1000, 'timestamp': timestamp}),
             headers={'Content-Type': 'application/json'})
    assert scs.model.getStatus('test9') == 'checked'

def test_addStats_setsStatusFixNeeded_whenOneTimeFailSpiderFailsAgain(app, db):
    timestamp = time.time()
    app.post('/add_stats', data=json.dumps({'name': 'test10', 'amount': 1000, 'timestamp': timestamp}),
             headers={'Content-Type': 'application/json'})
    timestamp += 24 * 3600
    app.post('/add_stats', data=json.dumps({'name': 'test10', 'amount': 1000, 'timestamp': timestamp}),
             headers={'Content-Type': 'application/json'})

    for i in range(2):
        timestamp += 24 * 3600
        app.post('/add_stats', data=json.dumps({'name': 'test10', 'amount': 100, 'timestamp': timestamp}),
                 headers={'Content-Type': 'application/json'})
        assert scs.model.getStatus('test10') == 'one-time-fail'

    timestamp += 24 * 3600
    app.post('/add_stats', data=json.dumps({'name': 'test10', 'amount': 500, 'timestamp': timestamp}),
             headers={'Content-Type': 'application/json'})
    assert scs.model.getStatus('test10') == 'fix-needed'

# checks the bug that was in 0.1.1
def test_addStats_doesntChangeStatus_whenFailedSpiderFailsAgain(app, db):
    timestamp = time.time()
    app.post('/add_stats', data=json.dumps({'name': 'test11', 'amount': 1000, 'timestamp': timestamp}),
             headers={'Content-Type': 'application/json'})
    timestamp += 24 * 3600
    app.post('/add_stats', data=json.dumps({'name': 'test11', 'amount': 1000, 'timestamp': timestamp}),
             headers={'Content-Type': 'application/json'})

    for i in range(2):
        timestamp += 24 * 3600
        app.post('/add_stats', data=json.dumps({'name': 'test11', 'amount': 100, 'timestamp': timestamp}),
                 headers={'Content-Type': 'application/json'})
        assert scs.model.getStatus('test11') == 'one-time-fail'

    for i in range(4):
        timestamp += 24 * 3600
        app.post('/add_stats', data=json.dumps({'name': 'test11', 'amount': 100, 'timestamp': timestamp}),
                 headers={'Content-Type': 'application/json'})
        assert scs.model.getStatus('test11') == 'fix-needed'

def test_addStats_doesntChangeStatus_whenStatusFailedSetManually(app, db):  # yeah, I need this too sometimes
    timestamp = time.time()
    app.post('/add_stats', data=json.dumps({'name': 'test12', 'amount': 1000, 'timestamp': timestamp}),
             headers={'Content-Type': 'application/json'})
    timestamp += 24 * 3600
    app.post('/add_stats', data=json.dumps({'name': 'test12', 'amount': 1000, 'timestamp': timestamp}),
             headers={'Content-Type': 'application/json'})

    app.post('/set_status', data=json.dumps({'name': 'test12', 'status': 'fix-needed'}),
             headers={'Content-Type': 'application/json'})

    for i in range(4):
        timestamp += 24 * 3600
        app.post('/add_stats', data=json.dumps({'name': 'test11', 'amount': 1000, 'timestamp': timestamp}),
                 headers={'Content-Type': 'application/json'})
        assert scs.model.getStatus('test11') == 'fix-needed'